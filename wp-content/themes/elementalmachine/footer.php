<?php

/**

 * The theme footer

 * 

 * @package em

 */
$footer_top_id = $wp_query->get_queried_object_id();
?>
</div><!--.site-content-->
</div><!--.container page-container-->
<?php 
$footer_top_meta = get_post_meta($footer_top_id, 'wpcf-cta-sections', true);
if((! empty( $footer_top_meta )) && ((!is_404()))) { ?>
<section class="section  section-call-to-action light footer_light" data-section="call-to-action">
    <div class="container">
        <div class="section-content">
            <div class="callout">
                <div class="callout-text col-md-6 col-xs-12">					
                    <?php
                    $footer_top = of_get_option('em_top_footer_text');
                    if($footer_top != NULL):
                        echo $footer_top;
                    endif;
                    ?>
                </div>
                <div class="callout-button col-md-6 col-xs-12">
                    <div class="em-footer-top-contact">
                        <h1><?php
                            $footer_heading = of_get_option('em_top_footer_heading');
                            if($footer_heading != NULL):  
                                echo $footer_heading; 
                            endif;?></h1>
                        <p><?php
                            $footer_content = of_get_option('em_top_footer_content');
                            if($footer_content != NULL):  
                                echo $footer_content;
                            endif;?>
                        </p>
                    </div>
                    <a href="#contact_form_pop" class="fancybox button primary medium">
                        <?php
                            $footer_btn_text = of_get_option('em_top_footer_btn_text');
                            if($footer_btn_text != NULL):  
                                echo $footer_btn_text; 
                            endif;?></a>
                    <div style="display:none" class="fancybox-hidden">
                        <div id="contact_form_pop">
                            <?php echo do_shortcode('[formidable id=4]');?></div>
                        </div>
                    <?php
//                  $footer_btn_txt = of_get_option('em_top_footer_btn_text');//
//                  $btn_link = of_get_option('em_top_footer_btn_link');//
//                  if($footer_btn_txt != NULL)://
                    ?>
                    <!-- <a href="<?php //if($btn_link != NULL): 
//                                            echo $btn_link;
//                                        else :
//                                            echo "#";
//                                        endif;?>" class="button primary medium"><?php //echo $footer_btn_txt;?></a> 
                                //<?php
//                                    endif;
//                                ?>
-->
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>
<section class="section section-footer-widgets dark footer_sec" data-section="footer-widgets">
    <div class="container">
        <div class="section-content row">
            <div class="col-sm-12 em-fotter-bottom-border">
                <div class="row">
                    <div class="em-footer-sidebar col-md-5 col-xs-12"><?php dynamic_sidebar('footer-1'); ?> </div>
                    <div class="em-footer-sidebar col-md-4 col-xs-12"><?php dynamic_sidebar('footer-2'); ?> </div>
                    <div class="em-footer-sidebar col-md-3 col-xs-12"><?php dynamic_sidebar('footer-3'); ?> </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section-footer-bottom dark footer_copy" data-section="footer-bottom">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <p class="footer-bottom-text">
                <?php
                $copyright = of_get_option('em_footer_copyright');
                    if($copyright != NULL):
                            echo $copyright;
                        endif;
                ?>
                </p>
            </div>
        </div>
    </div>
</section>
<!--wordpress footer-->
<?php wp_footer(); ?> 
</body>
</html>