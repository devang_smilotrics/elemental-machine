<?php
/**
 * Bootstrap Basic theme
 *
 * @package em
 */


/**

 * Required WordPress variable.

 */

if (!isset($content_width)) {

	$content_width = 1170;

}


if (!function_exists('emSetup')) {
	/**
	 * Setup theme and register support wp features.
	 */

	function emSetup()

	{

		/**

		 * Make theme available for translation

		 * Translations can be filed in the /languages/ directory

		 *

		 * copy from underscores theme

		 */

		load_theme_textdomain('em', get_template_directory() . '/languages');



		// add theme support post and comment automatic feed links

		add_theme_support('automatic-feed-links');



		// enable support for post thumbnail or feature image on posts and pages

		add_theme_support('post-thumbnails');



		// allow the use of html5 markup

		// @link https://codex.wordpress.org/Theme_Markup

		add_theme_support('html5', array('caption', 'comment-form', 'comment-list', 'gallery', 'search-form'));



		// add support menu

		register_nav_menus(array(

			'primary' => __('Primary Menu', 'em'),

		));



		// add post formats support

		add_theme_support('post-formats', array('aside', 'image', 'video', 'quote', 'link'));



		// add support custom background

		add_theme_support(

			'custom-background',

			apply_filters(

				'bootstrap_basic_custom_background_args',

				array(

					'default-color' => 'ffffff',

					'default-image' => ''

				)

			)

		);

	}// bootstrapBasicSetup

}

add_action('after_setup_theme', 'emSetup');





if (!function_exists('emWidgetsInit')) {

	/**

	 * Register widget areas

	 */

	function emWidgetsInit()

	{

		register_sidebar(array(

			'name'          => __('Header right', 'em'),

			'id'            => 'header-right',

			'before_widget' => '<div id="%1$s" class="widget %2$s">',

			'after_widget'  => '</div>',

			'before_title'  => '<h1 class="widget-title">',

			'after_title'   => '</h1>',

		));



		register_sidebar(array(

			'name'          => __('Navigation bar right', 'em'),

			'id'            => 'navbar-right',

			'before_widget' => '',

			'after_widget'  => '',

			'before_title'  => '',

			'after_title'   => '',

		));



		register_sidebar(array(

			'name'          => __('Sidebar left', 'em'),

			'id'            => 'sidebar-left',

			'before_widget' => '<aside id="%1$s" class="widget %2$s">',

			'after_widget'  => '</aside>',

			'before_title'  => '<h1 class="widget-title">',

			'after_title'   => '</h1>',

		));



		register_sidebar(array(

			'name'          => __('Sidebar right', 'em'),

			'id'            => 'sidebar-right',

			'before_widget' => '<aside id="%1$s" class="widget %2$s">',

			'after_widget'  => '</aside>',

			'before_title'  => '<h1 class="widget-title">',

			'after_title'   => '</h1>',

		));



		register_sidebar(array(

			'name'          => __('Footer 1', 'em'),

			'id'            => 'footer-1',

			'before_widget' => '<div id="%1$s" class="widget %2$s">',

			'after_widget'  => '</div>',

			'before_title'  => '<h1 class="widget-title">',

			'after_title'   => '</h1>',

		));



		register_sidebar(array(

			'name'          => __('Footer 2', 'em'),

			'id'            => 'footer-2',

			'before_widget' => '<div id="%1$s" class="widget %2$s">',

			'after_widget'  => '</div>',

			'before_title'  => '<h1 class="widget-title">',

			'after_title'   => '</h1>',

		));

        register_sidebar(array(

			'name'          => __('Footer 3', 'em'),

			'id'            => 'footer-3',

			'before_widget' => '<div id="%1$s" class="widget %2$s">',

			'after_widget'  => '</div>',

			'before_title'  => '<h1 class="widget-title">',

			'after_title'   => '</h1>',

		));

        register_sidebar(array(

			'name'          => __('Footer 4', 'em'),

			'id'            => 'footer-4',

			'before_widget' => '<div id="%1$s" class="widget %2$s">',

			'after_widget'  => '</div>',

			'before_title'  => '<h1 class="widget-title">',

			'after_title'   => '</h1>',

		));

		register_sidebar(array(

			'name'          => __('Footer Bottom', 'em'),

			'id'            => 'footer-bottom',

			'before_widget' => '<div id="%1$s" class="widget %2$s">',

			'after_widget'  => '</div>',

			'before_title'  => '<h1 class="widget-title">',

			'after_title'   => '</h1>',

		));



		//  New Room Sidebar

		register_sidebar( array(

		    'id'          => 'new-room',

		    'name'        => __( 'New Room Sidebar', 'em' ),

		    'description' => '',

		    'before_widget' => '<div id="%1$s" class="widget %2$s">',

			'after_widget'  => '</div>',

			'before_title'  => '<h1 class="widget-title">',

			'after_title'   => '</h1>',

		) );

		//  Press Release Sidebar

		register_sidebar( array(

		    'id'          => 'press-release-sidebar',

		    'name'        => __( 'Press Release Sidebar', 'em' ),

		    'description' => '',

		    'before_widget' => '<div id="%1$s" class="content-header widget-header v3 widget %2$s">',

		    'after_widget'  => '</div>',

		    'before_title'  => '<h2 class="press-release-sidebar">',

		    'after_title'   => '</h2>'

		) );





	}// bootstrapBasicWidgetsInit

}

add_action('widgets_init', 'emWidgetsInit');





if (!function_exists('emEnqueueScripts')) {

	/**

	 * Enqueue scripts & styles

	 */

	function emEnqueueScripts()

	{

		wp_enqueue_style('bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.6');

		wp_enqueue_style('bootstrap-style', get_template_directory_uri() . '/css/custom.css', array(), '3.3.6');

		wp_enqueue_style('bootstrap-theme-style', get_template_directory_uri() . '/css/bootstrap-theme.min.css', array(), '3.3.6');

		wp_enqueue_style('fontawesome-style', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '4.5.0');

		wp_enqueue_style('main-style', get_template_directory_uri() . '/css/main.css');
		
                wp_enqueue_style('custom-style', get_template_directory_uri() . '/css/custom-style.css');



		wp_enqueue_script('modernizr-script', get_template_directory_uri() . '/js/vendor/modernizr.min.js', array(), '3.3.1');

		wp_enqueue_script('respond-script', get_template_directory_uri() . '/js/vendor/respond.min.js', array(), '1.4.2');

		wp_enqueue_script('html5-shiv-script', get_template_directory_uri() . '/js/vendor/html5shiv.min.js', array(), '3.7.3');

		wp_enqueue_script('jquery');

		wp_enqueue_script('bootstrap-script', get_template_directory_uri() . '/js/vendor/bootstrap.min.js', array(), '3.3.6', true);

		wp_enqueue_script('main-script', get_template_directory_uri() . '/js/main.js', array(), false, true);


		wp_enqueue_style('em-style', get_stylesheet_uri());

	}// bootstrapBasicEnqueueScripts

}

add_action('wp_enqueue_scripts', 'emEnqueueScripts');


/* Add Css and js to footer for owl carousel */
function add_css_js_footer() {
    wp_enqueue_style( 'owl-carousel-css', get_template_directory_uri() . '/css/owl.carousel.css' );
    wp_enqueue_style( 'owl-theme', get_template_directory_uri() . '/css/owl.theme.css' );
    wp_enqueue_script('owl-carousel-script', get_template_directory_uri() . '/js/owl.carousel.js');
   

    ?>
   	<script>
		    jQuery(document).ready(function() {
		    	
		      jQuery("#home-solutions-slider").owlCarousel({
		        items : 3,
		        itemsDesktop : [1199,3],
		        itemsDesktopSmall : [979,3],
                        
		      });
                      jQuery("#home-products-slider").owlCarousel({
		        items : 1,
		        itemsDesktop : [1199,3],
		        itemsDesktopSmall : [979,3],
                        navigation: true,
                        navigationText: ["<",">"],
                        pagination : false, 
                        
                        
		      });
                      jQuery(".fancybox").fancybox({'width':700 ,
                         'autoSize' : false});

    		});
    </script>
    <style>
    #home-solutions-slider .solution-contents .item{
        margin: 5px;
        color: #FFF;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }
    #home-products-slider .solution-contents .item{
        margin: 5px;
        color: #FFF;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }
    </style>
    <?php

};
add_action( 'wp_footer', 'add_css_js_footer' );


/**

 * admin page displaying help.

 */

if (is_admin()) {

	require get_template_directory() . '/inc/BootstrapBasicAdminHelp.php';

	$bbsc_adminhelp = new BootstrapBasicAdminHelp();

	add_action('admin_menu', array($bbsc_adminhelp, 'themeHelpMenu'));

	unset($bbsc_adminhelp);

}





/**

 * Custom template tags for this theme.

 */

require get_template_directory() . '/inc/template-tags.php';





/**

 * Custom functions that act independently of the theme templates.

 */

require get_template_directory() . '/inc/extras.php';





/**

 * Custom dropdown menu and navbar in walker class

 */

require get_template_directory() . '/inc/BootstrapBasicMyWalkerNavMenu.php';





/**

 * Template functions

 */

require get_template_directory() . '/inc/template-functions.php';





/**

 * --------------------------------------------------------------

 * Theme widget & widget hooks

 * --------------------------------------------------------------

 */

require get_template_directory() . '/inc/widgets/BootstrapBasicSearchWidget.php';

require get_template_directory() . '/inc/template-widgets-hook.php';

require get_template_directory() . '/widget_pressrelease.php';



/* Filter for execute the shortcode in widget */



add_filter('widget_text', 'do_shortcode');





/*Add js for sicky header */





    function sticky_header_func() {

   		?>

   			<script type='text/javascript'>

   				jQuery( document ).ready(function() {

    				jQuery(window).scroll(function(){

						  var sticky = jQuery('.sticky'),

						      scroll = jQuery(window).scrollTop();



						  if (scroll >= 100) sticky.addClass('fixed');

						  else sticky.removeClass('fixed');

						});

				});

   			</script>

   		<?php

   	 }

    add_action( 'wp_footer', 'sticky_header_func', 100 );





 function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


function fix_svg_thumb_display() {
  echo '
    td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail {
      width: 100% !important;
      height: auto !important;
    }
  ';
}
//add_action('admin_head', 'fix_svg_thumb_display');


add_action( 'init', 'setting_utm_cookie' );

function setting_utm_cookie() {
		$utm_code = array();

        if(isset($_GET['utm_campaign'])){
        	$utm_code['utm_campaign'] = $_GET['utm_campaign'];
        }

        if(isset($_GET['utm_source'])){
        	$utm_code['utm_source'] = $_GET['utm_source'];
        }

        if(isset($_GET['utm_medium'])){
        	$utm_code['utm_medium'] = $_GET['utm_medium'];
        }

        $cookie_name = "utm_code";
        $cookie_value = serialize($utm_code);

       	if(!isset($_COOKIE[$cookie_name])) {
  			setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/" );
  		}
}

add_action('frm_after_create_entry', 'update_to_zoho_crm', 30, 2);
function update_to_zoho_crm($entry_id, $form_id){

    if(isset($form_id)) {

	    $body = '';
	    $body .= 'UTM Campaign: '.$_SESSION['utm_campaign'].'<br />';
	    $body .= 'UTM Source: '.$_SESSION['utm_source'].'<br />';
	    $body .= 'UTM Medium: '.$_SESSION['utm_medium'].'<br />';
	   
	    if($form_id == '4'){
	        
	        $body .= 'First Name: '.$_POST['item_meta'][9].'<br />';
	        $body .= 'Last Name: '.$_POST['item_meta'][10].'<br />';
	       
	       
	       /*  Zoho Crm insert leads*/
	       $ctype = $_POST['item_meta'][19];
		   $company_type = implode(",",$ctype);
	        
	        $xmlData = '<Leads>
						<row no="1">
						<FL val="First Name">'.$_POST['item_meta'][9].'</FL>
						<FL val="Last Name">'.$_POST['item_meta'][10].'</FL>
						<FL val="Email">'.$_POST['item_meta'][11].'</FL>
						<FL val="Phone">'.$_POST['item_meta'][12].'</FL>
						<FL val="Country">'.$_POST['item_meta'][13].'</FL>
						<FL val="Title">'.$_POST['item_meta'][17].'</FL>
						<FL val="Company">'.$_POST['item_meta'][18].'</FL>
						<FL val="Description">'.$_POST['item_meta'][89].'</FL>
						<FL val="Form Name">Contact Us</FL>
						<FL val="Site Name">'.$_SERVER['HTTP_HOST'].'</FL>
						<FL val="Company Type">'.$company_type.'</FL>';

					
	
	    }
	    if($form_id == '5'){
            
            $body .= 'First Name: '.$_POST['item_meta'][28].'<br />';
	        $body .= 'Last Name: '.$_POST['item_meta'][29].'<br />';
	       
	       
	       /*  Zoho Crm insert leads*/
	        $xmlData = '<Leads>
						<row no="1">
						<FL val="First Name">'.$_POST['item_meta'][28].'</FL>
						<FL val="Last Name">'.$_POST['item_meta'][29].'</FL>
						<FL val="Email">'.$_POST['item_meta'][30].'</FL>
						<FL val="Company">'.$_POST['item_meta'][31].'</FL>
						<FL val="Form Name">'.$_POST['item_meta'][91].'</FL>
						<FL val="Site Name">'.$_SERVER['HTTP_HOST'].'</FL>';

	    }
	    if($form_id == '10'){
            
            $body .= 'First Name: '.$_POST['item_meta'][97].'<br />';
	        $body .= 'Last Name: '.$_POST['item_meta'][98].'<br />';
	       
	       
	       /*  Zoho Crm insert leads*/
	        $xmlData = '<Leads>
						<row no="1">
						<FL val="First Name">'.$_POST['item_meta'][97].'</FL>
						<FL val="Last Name">'.$_POST['item_meta'][98].'</FL>
						<FL val="Email">'.$_POST['item_meta'][99].'</FL>
						<FL val="Company">'.$_POST['item_meta'][100].'</FL>
						<FL val="Form Name">'.$_POST['item_meta'][101].'</FL>
						<FL val="Site Name">'.$_SERVER['HTTP_HOST'].'</FL>';


	    }
	    if($form_id == '9'){
            
            $body .= 'First Name: '.$_POST['item_meta'][92].'<br />';
	        $body .= 'Last Name: '.$_POST['item_meta'][93].'<br />';
	       
	       
	       /*  Zoho Crm insert leads*/
	        $xmlData = '<Leads>
						<row no="1">
						<FL val="First Name">'.$_POST['item_meta'][92].'</FL>
						<FL val="Last Name">'.$_POST['item_meta'][93].'</FL>
						<FL val="Email">'.$_POST['item_meta'][94].'</FL>
						<FL val="Company">'.$_POST['item_meta'][95].'</FL>
						<FL val="Form Name">'.$_POST['item_meta'][96].'</FL>
						<FL val="Site Name">'.$_SERVER['HTTP_HOST'].'</FL>';


						
	    }
	    if($form_id == '2'){
            
            $body .= 'First Name: '.$_POST['item_meta'][8].'<br />';
	        $body .= 'Last Name: '.$_POST['item_meta'][20].'<br />';
	       
	       
	       /*  Zoho Crm insert leads*/
	        $xmlData = '<Leads>
						<row no="1">
						<FL val="First Name">'.$_POST['item_meta'][8].'</FL>
						<FL val="Last Name">'.$_POST['item_meta'][20].'</FL>
						<FL val="Email">'.$_POST['item_meta'][21].'</FL>
						<FL val="Phone">'.$_POST['item_meta'][22].'</FL>
						<FL val="Description">'.$_POST['item_meta'][24].'</FL>
						<FL val="Form Name">Job Application</FL>
						<FL val="Site Name">'.$_SERVER['HTTP_HOST'].'</FL>';

						
	    }

	       $xmlData .=  '<FL val="UTM Campaign">'.$_SESSION['utm_campaign'].'</FL>
						 <FL val="UTM Medium">'.$_SESSION['utm_source'].'</FL>
						 <FL val="UTM Source">'.$_SESSION['utm_medium'].'</FL>
						 <FL val="Lead Source">Website</FL>
						 <FL val="Lead Source Detail">Web Form</FL>

						</row>
					    </Leads>';
 
            $auth_token="e44b88a12b9e9ed7272d64af4e8dfab6";
		    $url = "https://crm.zoho.com/crm/private/xml/Leads/insertRecords?authtoken=".$auth_token."&scope=crmapi";
		    $post=array("newFormat"=>'1',"xmlData"=>$xmlData);
			//================= start curl ===================
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch,CURLOPT_POSTFIELDS,$post);
			$result = curl_exec($ch);
			curl_close($ch);
			//================= end curl ===================

	        $to = 'riteshvatwani@gmail.com';
	        $headers[] = 'Content-Type: text/html; charset=UTF-8';
	        $headers[] = 'Cc: James <james@traversedm.com>';
	        $subject = 'Zoho Fields';

	       // wp_mail( $to, $subject, $body, $headers );
	}

}

function _remove_script_version( $src ){
	$parts = explode( '?ver', $src );
	return $parts[0].'?ver=1.1';
}
 add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
 add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );
