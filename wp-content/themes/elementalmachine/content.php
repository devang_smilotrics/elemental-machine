<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
      <?php if($post_type == 'product' || $post_type == 'job' ) {} else { ?>
      <header class="entry-header">
            <?php if ('post' == get_post_type()) { ?> 
            <div class="entry-date"><a href="<?php echo get_post_permalink(); ?>"><?php echo get_the_date( 'F j, Y' ); ?></a> </div>
            <?php } //endif; ?> 
            
          
            <a href="<?php echo get_the_permalink(); ?>"><h2 class="entry-title"><?php the_title(); ?></h2></a>   
                <div class="entry-meta">
                   <span>Posted by</span>
                   <span class="entry-author"><?php echo get_the_author();?></span>
                   <span>in</span>
                   <span class="entry-cats">
                       <?php
                       $categories_list = get_the_category_list(__(', ', 'em'));
                       if (!empty($categories_list)) {
                       ?> 
                           <span class="cat-links">
                           <?php echo $categories_list; ?> 
                           </span>
                      <?php } ?>
                   </span>
               </div>   
            
	</header><!-- .entry-header -->
        <?php } ?>
         
        
        <?php if($post_type == 'product' || $post_type == 'job' ) {} else {?>
        <section class="entry-featured">
            <figure><?php the_post_thumbnail ();?></figure>
        </section>
        <?php } ?>
        
	
	<section class="entry-summary post-content">
		<?php strip_tags(the_content(bootstrapBasicMoreLinkText())); ?> 
		<div class="clearfix"></div>
		<?php 
		
		wp_link_pages(array(
			'before' => '<div class="page-links">' . __('Pages:', 'em') . ' <ul class="pagination">',
			'after'  => '</ul></div>',
			'separator' => ''
		));
		?> 
	</section><!-- .entry-content -->	

</article><!-- #post-## -->