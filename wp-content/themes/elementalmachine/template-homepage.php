<?php
/**
 * Template Name: Home page
 */

get_header();
//$sidebar_active = get_post_meta(get_the_ID(),'wpcf-page-sidebar');
////var_dump($sidebar_active);
//$main_column_size = bootstrapBasicGetMainColumnSize(get_the_ID());

//var_dump($main_column_size);
?> 
				<div class="content-area" id="main-column">
					<main id="main" class="site-main" role="main">
						<?php 
						while (have_posts()) {
							the_post();

							get_template_part('content', 'page');

							echo "\n\n";
							
							// If comments are open or we have at least one comment, load up the comment template
							if (comments_open() || '0' != get_comments_number()) {
								comments_template();
							}

							echo "\n\n";

						} //endwhile;
						?> 
					</main>
				</div>
<?php get_footer(); ?> 