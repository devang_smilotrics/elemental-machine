<?php
/**
 * Template Name: New Room - Articles List
 */

get_header();
$img = get_post_meta(get_the_ID(),'wpcf-header-image');
if($img[0]!=''){
?>
    <div class="page-title" style="background: url(<?php echo $img[0]?>); background-size:cover;">
<?php }else{ ?>
    
    <div class="page-title">
<?php } ?> 
            <div class="container">
		<h1 class="page-heading"><?php the_title(); ?></h1>
            </div>
</div><!-- .page-title -->

<section id="main-content" class="section light">
	<div class="container">
		<div class="section-content">
			<div id="primary" class="col-sm-9">
                            <h2 class="vc_custom_heading">Articles</h2>
                            <?php
                            $type = 'article';
                            $args=array(
                            'post_type' => $type,
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                            'orderby'   => 'wpcf-featured-article',
                            'meta_key'  => 'wpcf-featured-article',
                            'meta_value'  => 1,
                            'order'     => 'ASC',
                            'caller_get_posts' => 1
                            );
                            
                            $my_query = null;
                            $my_query = new WP_Query($args);
                            if ($my_query->have_posts()) {
                                while ($my_query->have_posts()) : $my_query->the_post();

                                $featured_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                                ?>
                            <div class="wpb_text_column wpb_content_element wpb_cust_article article-content">
                                <div class="wpb_wrapper">
                                    
									<?php if($featured_image != NULL): ?>
                                       <p><img class="alignnone size-full" src="<?php echo $featured_image; ?>" /></p>
                                    <?php endif; ?>
                                           <?php
                                        $article_live_url = get_post_meta(get_the_ID(),'wpcf-article-url');
                                       ?>
                                       <?php if($article_live_url[0] != NULL ): ?>
                                            <h3><a target="_blank" href="<?php echo $article_live_url[0]; ?>"><?php the_title(); ?></a></h3>
                                       <?php else: ?>
                                             <h3><a target="_blank" href="#"><?php the_title(); ?></a></h3>
                                       <?php endif; ?>
                                      <?php echo get_the_date(); ?>       
                                    
                                </div>
                            </div>
                            <?php
                                    endwhile;
                                }
                                wp_reset_query();  // Restore global post data stomped by the_post().
                            ?>
                            <?php
                            $type = 'article';
                            $args=array(
                            'post_type' => $type,
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                            'meta_key'  => 'wpcf-featured-article',
                            'meta_value'  => 0,
                            'caller_get_posts' => 1
                            );

                            $my_query = null;
                            $my_query = new WP_Query($args);
                            if ($my_query->have_posts()) {
                                while ($my_query->have_posts()) : $my_query->the_post();

                                $featured_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                                ?>
                            <div class="wpb_text_column wpb_content_element wpb_cust_article article-content">
                                <div class="wpb_wrapper">
                                    <?php if($featured_image != NULL): ?>
                                       <p><img class="alignnone size-full" src="<?php echo $featured_image; ?>" /></p>
                                    <?php endif; ?>
                                           <?php
                                        $article_live_url = get_post_meta(get_the_ID(),'wpcf-article-url');
                                       ?>
                                       <?php if($article_live_url[0] != NULL ): ?>
                                            <h3><a target="_blank" href="<?php echo $article_live_url[0]; ?>"><?php the_title(); ?></a></h3>
                                       <?php else: ?>
                                             <h3><a target="_blank" href="#"><?php the_title(); ?></a></h3>
                                       <?php endif; ?>

                                    <?php echo get_the_date(); ?>
                                </div>
                            </div>
                            <?php
                                    endwhile;
                                }
                                wp_reset_query();  // Restore global post data stomped by the_post().
                            ?>
			</div>
			<div id="secondary" class="col-sm-3">
				<?php dynamic_sidebar( 'new-room' ); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>