<?php get_header(); ?> 

				<div class="col-md-12 content-area" id="main-column">
					<main id="main" class="site-main" role="main">
						<section class="error-404 not-found">
                                                    <header class="page-header">
                                                            <h1 class="page-title"><?php echo of_get_option('em_pnf_head'); ?></h1>
                                                            <?php echo of_get_option('em_pnf_content'); ?>
                                                    </header><!-- .page-header -->
						</section><!-- .error-404 -->
					</main>
				</div>

<?php get_footer(); ?> 