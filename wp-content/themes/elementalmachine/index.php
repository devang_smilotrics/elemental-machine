<?php
/**
 * The main template file
 * 
 * @package em
 */

get_header();

/**
 * determine main column size from actived sidebar
 */
$main_column_size = bootstrapBasicGetMainColumnSize(get_the_ID());
 //$sidebar_active = get_post_meta(get_the_ID(),'wpcf_page-sidebar');
 //var_dump($sidebar_active);
$blog_id = get_option('page_for_posts');
$img = get_post_meta($blog_id,'wpcf-header-image');
if(is_home()){
?>
    <div class="page-title" style="background: url(<?php echo $img[0]?>); background-size:cover;">
<?php }else{ ?>
    <div class="page-title">
<?php } ?> 
            <div class="container">
				<h1 class="page-heading"><?php if(is_home()){ echo "Blog"; }else{the_title();} ?></h1>
            </div>
    </div><!-- .page-title -->

<div class="container">
               
				<div class="col-md-9 content-area blog-post" id="main-column">
					<main id="main" class="site-main" role="main">
						<?php if (have_posts()) { ?> 
						<?php 
						// start the loop
						while (have_posts()) {
							the_post();
							
							/* Include the Post-Format-specific template for the content.
							* If you want to override this in a child theme, then include a file
							* called content-___.php (where ___ is the Post Format name) and that will be used instead.
							*/
							get_template_part('content', get_post_format());
						}// end while
						
						bootstrapBasicPagination();
						?> 
						<?php } else { ?> 
						<?php get_template_part('no-results', 'index'); ?>
						<?php } // endif; ?> 
					</main>
				</div>
                            <?php get_sidebar('right'); ?> 
                        </div>
<?php get_footer(); ?> 