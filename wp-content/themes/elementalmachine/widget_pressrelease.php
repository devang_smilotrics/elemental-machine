<?php

// Creating the widget 
class wpbpr_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                        // Base ID of your widget
                        'wpbpr_widget',
                        // Widget name will appear in UI
                        __('Latest Press Release', 'em'),
                        // Widget description
                        array('description' => __('List the Latest Press Releases', 'em'),)
        );
    }

// Creating widget front-end
// This is where the action happens
    public function widget($args, $instance) {
        $title = apply_filters('widget_title', $instance['title']);

        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];
        $type = 'pressrelease';
        $pr_args = array(
            'post_type' => $type,
            'post_status' => 'publish',
            'posts_per_page' => $instance['pr_post_count'],
            'caller_get_posts' => 1
        );

        $press_release = null;
        $press_release = new WP_Query($pr_args);
        if ($press_release->have_posts()) {
            while ($press_release->have_posts()) : $press_release->the_post();
?>
                <p><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>
<?php
                endwhile;
            }
            wp_reset_query();  // Restore global post data stomped by the_post().
            if ($instance['show_all_link'] != NULL) {
                echo '<a style="display:block;margin: 15px 0;" href="' . $instance['show_all_link'] . '" >All Press Releases</a>';
            }
            // This is where you run the code and display the output
            echo $args['after_widget'];
        }

// Widget Backend
        public function form($instance) {
            if (isset($instance['title'])) {
                $title = $instance['title'];
            } else {
                $title = __('Press Release', 'em');
            }
            $pr_post_count = $instance['pr_post_count'];
            $show_all_link = $instance['show_all_link'];
            // Widget admin form
?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('pr_post_count'); ?>"><?php _e('Count:'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('pr_post_count'); ?>" name="<?php echo $this->get_field_name('pr_post_count'); ?>" type="text" value="<?php echo esc_attr($pr_post_count); ?>" />
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('show_all_link'); ?>"><?php _e('Press Release Link:'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('show_all_link'); ?>" name="<?php echo $this->get_field_name('show_all_link'); ?>" type="text" value="<?php echo esc_attr($show_all_link); ?>" />
            </p>
<?php
        }

        // Updating widget replacing old instances with new
        public function update($new_instance, $old_instance) {
            $instance = array();
            $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
            $instance['pr_post_count'] = (!empty($new_instance['pr_post_count']) ) ? strip_tags($new_instance['pr_post_count']) : '';
            $instance['show_all_link'] = (!empty($new_instance['show_all_link']) ) ? strip_tags($new_instance['show_all_link']) : '';
            return $instance;
        }

    }

    // Class wpb_widget ends here
// Register and load the widget
    function wpb_load_widget() {
        register_widget('wpbpr_widget');
    }

    add_action('widgets_init', 'wpb_load_widget');
?>