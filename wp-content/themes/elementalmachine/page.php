<?php
/**
 * Template for displaying pages
 * 
 * @package em
 */

get_header();

/**
 * determine main column size from actived sidebar
 */
$page_id = get_the_ID();
$sidebar_active = get_post_meta($page_id,'wpcf-page-sidebar');
$main_column_size = bootstrapBasicGetMainColumnSize($page_id);

?>
                <div class="container">
                <?php if($sidebar_active[0] == '2'){ get_sidebar('left'); }?>
				<div class="col-md-<?php echo $main_column_size; ?> content-area" id="main-column">
					<main id="main" class="site-main" role="main">
						<?php 
						while (have_posts()) {
							the_post();

							get_template_part('content', 'page');

							echo "\n\n";
							
							// If comments are open or we have at least one comment, load up the comment template
							if (comments_open() || '0' != get_comments_number()) {
								comments_template();
							}

							echo "\n\n";

						} //endwhile;
						?> 
					</main>
				</div>
               <?php if($sidebar_active[0] == '3'){ get_sidebar('right'); } ?>
                         </div>
<?php get_footer(); ?> 