<?php
/**
 * Template for displaying single post (read full post page).
 * 
 * @package em
 */

get_header();

/**
 * determine main column size from actived sidebar
 */
$main_column_size = 9;
?>
<?php if($post_type == 'post' || $post_type == 'job' || $post_type == 'solution-slider' ) {} else { ?>
<div class="page-title" style="background: url(<?php echo get_template_directory_uri(); ?>/img/header_image_hero.png); background-size:cover;">
    <div class="container">
        <h1 class="page-heading">Blog</h1>
    </div>
</div>
<?php } ?>
            <div class="<?php if($post_type == 'product'){echo "content-area";}else { echo "container";}?>">
				<div class="col-md-<?php echo $main_column_size; ?> content-area" id="main-column">
					<main id="main" class="site-main" role="main">
						<?php 
						while (have_posts()) {
							the_post();

							get_template_part('content', get_post_format());

							echo "\n\n";
							
							bootstrapBasicPagination();

							echo "\n\n";
							
							// If comments are open or we have at least one comment, load up the comment template
							if (comments_open() || '0' != get_comments_number()) {
								comments_template();
							}

							echo "\n\n";

						} //endwhile;
						?> 
					</main>
				</div>
                            <?php 
                            $post_type_arr = array("product", "job");
                               ?>
                            <?php if(!in_array($post_type, $post_type_arr)){ get_sidebar('right');} ?>
                         </div>
<?php get_footer(); ?> 
