<?php
/**
 * Template Name: with right sidebar
 */

get_header();
$img = get_post_meta(get_the_ID(),'wpcf-header-image');
if($img[0]!=''){
?>
    <div class="page-title" style="background: url(<?php echo $img[0]?>); background-size:cover;">
<?php }else{ ?>
    
    <div class="page-title">
<?php } ?> 
            <div class="container">
		<h1 class="page-heading"><?php the_title(); ?></h1>
            </div>
</div><!-- .page-title -->

<section id="main-content" class="section light">
	<div class="container">
		<div class="section-content">
			<div id="primary" class="col-sm-9">
                            <h2 class="vc_custom_heading"><?php the_title(); ?></h2>
                            <div class="cotent"><?php the_content(); ?></div>
			</div>
			<div id="secondary" class="col-sm-3">
				<?php dynamic_sidebar( 'new-room' ); ?>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>