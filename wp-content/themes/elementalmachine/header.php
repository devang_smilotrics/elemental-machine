<?php
/**
 * The theme header
 * 
 * @package em
 */
?>
<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>     <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>     <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><?php wp_title('|', true, 'right'); ?></title>
		<meta name="viewport" content="width=device-width">

		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
                <link rel="shortcut icon" href="<?php echo get_template_directory_uri ();?>/img/favicon.ico">
		<link href='https://fonts.googleapis.com/css?family=Raleway|Open+Sans' rel='stylesheet' type='text/css'>

		<!--wordpress head-->
		<?php wp_head(); ?>
        
        
	</head>
	<?php 
		
        ## UTM Code ##
		// Track UTM Google Ads
		$utm_code = array();
        if(isset($_GET['utm_campaign'])){
            $_SESSION['utm_campaign'] = $_GET['utm_campaign'];
		}

        if(isset($_GET['utm_source'])){
            $_SESSION['utm_source'] = $_GET['utm_source'];
		}

        if(isset($_GET['utm_medium'])){
            $_SESSION['utm_medium'] = $_GET['utm_medium'];
		}
		
		## UTM Code ##
	?>
	<body <?php body_class(); ?>>
		<!--[if lt IE 8]>
			<p class="ancient-browser-alert">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" target="_blank">upgrade your browser</a>.</p>
		<![endif]-->
		
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-58DC7T"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-58DC7T');</script>
<!-- End Google Tag Manager -->		
		
		
		<div class="page-container">
			<?php do_action('before'); ?> 
			<header role="banner" class="em-header sticky">

			  <div class="container">  
				<div class="row row-with-vspace site-branding">
					<div class="col-sm-4 col-md-4 col-xs-12 site-title">
						<div id="logo">
                            <p class="site-title">
                            	<?php
                                $logo = of_get_option('em_logo');
                                ?>
                                <a href="<?php echo get_home_url(); ?>" class="standard-logo" data-dark-logo="<?php echo $logo; ?>"><img src="<?php echo $logo; ?>" alt="Elemental Machines Logo"></a>
                                
                        	</p>
                        </div>
						
					</div>
					<div class="col-sm-8 col-xs-12 col-md-8 page-header-top-right">
                         <nav class="navbar navbar-default" role="navigation">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-primary-collapse">
									<span class="sr-only"><?php _e('Toggle navigation', 'em'); ?></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
                                                            
                                                                <div class="collapse navbar-collapse navbar-primary-collapse">
                                                                    <?php wp_nav_menu(array('theme_location' => 'primary', 'container' => false, 'menu_class' => 'nav navbar-nav desktop', 'walker' => new BootstrapBasicMyWalkerNavMenu())); ?> 
                                                                    
                                                                     <?php wp_nav_menu(array('theme_location' => 'primary', 'container' => false, 'menu_class' => 'nav navbar-nav mobile', 'walker' => new BootstrapBasicMyWalkerNavMenu())); ?> 
                                                                </div>  
                                                            
                                                            
                                                                
							</div>
                                            </nav>
                                             
					</div>
					<!--<div class="col-md-2 head-buynow-btn page-header-top-right">
						<a href="#">BUY NOW </a>
					</div>-->
             	</div>                        
			</header>
			
			
                    <div id="content" class="row-with-vspace site-content" style="overflow: hidden">