/**
 * Main Javascript.
 * This file is for who want to make this theme as a new parent theme and you are ready to code your js here.
 */
jQuery(document).ready(function(){
    var x_width = screen.width;

   if(x_width > 769){
        jQuery('.mobile .dropdown > a').addClass('dropdown-toggle');
        jQuery('.mobile .dropdown > a').attr('data-toggle','dropdown');
   }
   
// start For mobile
        jQuery('.mobile .dropdown > a .caret ').css('display','none');
        jQuery('.mobile .dropdown > a').addClass('mobile_a');
        jQuery('.mobile .dropdown > a').after('<span class="mobile_caret"> + </span>');
        
        jQuery(".mobile .dropdown .mobile_caret").click(function() {  
            jQuery(".mobile .dropdown").removeClass('open');
            jQuery(this).parent(".dropdown").addClass('open');
        });
// End for mobile

    jQuery(".dropdown, .btn-group").hover(function(){
       
        if(x_width > 769){
            var dropdownMenu = jQuery(this).children(".dropdown-menu");
            if(dropdownMenu.is(":visible")){
                dropdownMenu.parent().toggleClass("open");
            }
        }
    });
});