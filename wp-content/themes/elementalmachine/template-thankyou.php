<?php
/**
 * Template Name: ThankYou page
 */

get_header();

$img = get_post_meta(get_the_ID(),'wpcf-header-image');
if($img[0]!=''){
?>
    <div class="page-title" style="background: url(<?php echo $img[0]?>); background-size:cover;">
<?php }else{ ?>
    
    <div class="page-title">
<?php } ?> 
            <div class="container">
		<h1 class="page-heading"><?php the_title(); ?></h1>
            </div>
    </div><!-- .page-title -->


		<div class="container">
      <div class="col-md-12 content-area" id="main-column">
            <main id="main" class="site-main" role="main">
              <?php the_content(); ?>
              <?php  if(isset($_GET['id'])) { 

                  $download_id = $_GET['id'];
              ?>
                      <strong>Click the Download Resource link below to open the file:</strong> 
                      <div class="vc_btn3-container vc_btn3-inline thnkdown">
                         <a target="blank" class="vc_general vc_btn3 vc_btn3-size-sm vc_btn3-shape-rounded vc_btn3-style-classic vc_btn3-color-turquoise" href="<?php echo get_site_url().'/download.php?download_id='.$_GET['id']; ?>">Download the White Paper</a></div>

              <?php }  ?>
            </main>
      </div>
    </div>
    <script>
          jQuery( document ).ready(function() {
          //jQuery('#downloadform').featherlight();
          <?php if(isset($_GET['id'])) { ?>
          window.open("<?php echo get_site_url().'/download.php?download_id='.$_GET['id']; ?>","_newtab");
          <?php } ?> });
    </script>
<?php get_footer(); ?> 