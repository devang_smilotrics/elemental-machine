<?php
/*
Plugin Name: Elemental Machine Shortcodes
Plugin URI: elementalmachines.io
Description: Extend theme with custom shortcodes.
Version: 0.1.0
Author: Elemental Machine
License: GPLv2 or later
*/

// News List
function footer_news_func( $atts ) {

    $args = array(
          'post_type' => 'post',
          'posts_per_page' => 1 ,
          'order' => 'DESC', 
          'orderby' => 'date', 
          // all posts
    );
    $project = new WP_Query( $args );
    if( $project->have_posts() ) {
        $output .= ' <div class="footer-news-list">';

       

          while( $project->have_posts() ) {
            $project->the_post();
            
                $output .= '<div class="news-contents">
                                <div class="latest-text">
                                        <div class="border-line">
                                            <a href='.get_the_permalink().'><h4>'.get_the_title().'</h4></a>
                                            <p>'.wp_trim_words( get_the_content(), 40 ) .'</p>
                                            <a href='.get_the_permalink().' class="read-more">Read this post<i class="fa fa-chevron-right" aria-hidden="true"></i></a>
                                        </div>
                                </div>                                
                            </div>';
           
            
          }
         
        $output .= '</div>'; 
        }else{
            $output = 'Please add Projects from backend';
        }
     return $output;
}
add_shortcode( 'footer_news', 'footer_news_func' );

// Home solution Slider List
function home_solution_func( $atts ) {

    $args = array(
          'post_type' => 'solution-slider',
          'posts_per_page' => 4 ,
          'order' => 'ASC', 
          'orderby' => 'date', 
          // all posts
    );
    $project = new WP_Query( $args );
    if( $project->have_posts() ) {
        $output .= ' <div id="home-solutions-slider" class="home-solutions owl-carousel>
                       <div class="row">';

       

          while( $project->have_posts() ) {
            $project->the_post();
            
                $output .= '<div class="solution-contents">
                                <div class="item content-area">
                                        <div class="solution-thumb"><img src="'.wp_get_attachment_url( get_post_thumbnail_id()).'"/></div>
                                        <div class="border-line">
                                            <a href='.get_the_permalink().'><h4>'.get_the_title().'</h4></a>
                                        </div>
                                </div>                                
                            </div>';
          }
         
        $output .= '</div ></div>'; 
        }else{
            $output = 'Please add solution from backend';
        }
     return $output;
}
add_shortcode( 'home_solution', 'home_solution_func' );


//  Solutions List
function solution_func( $atts ) {

    $args = array(
          'post_type' => 'solution-slider',
          'posts_per_page' => -1 ,
          'order' => 'ASC', 
          
          // all posts
    );
    $project = new WP_Query( $args );
    if( $project->have_posts() ) {
        $output .= ' <div id="solutions-list">
                       <div class="content-area">';

        $postcounter = 0; 
 
          while( $project->have_posts() ) {
            $project->the_post();
              if($postcounter == 0){
                    $output .= '<div class="row solution-blocks">';
                    
                  }     
                $output .= '<div class="col-md-6 solution-main-area">
                                <div class="solution-thumb col-md-2 col-sm-2"><a href="'.get_permalink().'"><img src="'.wp_get_attachment_url( get_post_thumbnail_id()).'"/></a></div>
                                <div class="sol-right-contents col-md-10 col-sm-10">
                                        <div class="sol-title"><a href="'.get_permalink().'"><h2>'.get_the_title().'</h2></a></div>
                                        <div class="sol-contents">'.get_the_excerpt().'</div>
                                </div>                                
                            </div>';
                $postcounter++;

                              if ( 2 == $postcounter ) {
                                $output .= '</div>';
                                $postcounter = 0;  
                              }
          }
         
        $output .= '</div></div>'; 
        }else{
            $output = 'Please add solution from backend';
        }
     return $output;
}
add_shortcode( 'solution_list', 'solution_func' );



function em_social() {
    $twitter = of_get_option('em_twitter_link');
    $fb = of_get_option('em_facebook_link');
    $googleplus = of_get_option('em_g_plus_link');
    $linkedin = of_get_option('em_linkedin_link');
    
    $icons = "";
    if ($fb != NULL):
        $icons .= '<a href="'.$fb.'" class="footer-social"><i class="fa fa-facebook" aria-hidden="true"></i></a>';
    endif;
    if ($twitter != NULL):
        $icons .= '<a href="'.$twitter.'" class="footer-social"><i class="fa fa-twitter" aria-hidden="true"></i></a>';
    endif;
    if ($linkedin != NULL):
        $icons .= '<a href="'.$linkedin.'" class="footer-social"><i class="fa fa-linkedin" aria-hidden="true"></i></a>';
    endif;
    if ($googleplus != NULL):
        $icons .= '<a href="'.$googleplus.'" class="footer-social"><i class="fa fa-google-plus" aria-hidden="true"></i></a>';
    endif;
    
    return $icons;
}
add_shortcode( 'em_social_icons' , 'em_social' );

/* Add Metabox for download the white papers and Case study.*/

function add_custom_meta_box()

{
    $post_types = array ( 'post', 'page','white-paper','case-study');

    add_meta_box("demo-meta-box", "Whitepaper & Case study", "custom_meta_box_markup", $post_types, "side", "high", null);
}

add_action("add_meta_boxes", "add_custom_meta_box");


/* Add th field on teh meta box.*/
function custom_meta_box_markup($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");

    ?>
        <div>
            
            <label for="meta-box-checkbox">White Paper</label>
            <?php
                $checkbox_whitepaper_value = get_post_meta($object->ID, "white-paper-checkbox", true);
                $checkbox_casestudy_value = get_post_meta($object->ID, "case-study-checkbox", true);
                /* White Paper*/
                
                if($checkbox_whitepaper_value  == "")
                {
                    ?>
                        <input name="white-paper-checkbox" type="checkbox" value="true">
                    <?php
                }
                else if($checkbox_whitepaper_value  == "true")
                {
                    ?>
                    <input name="white-paper-checkbox" type="checkbox" value="true" checked>
                    <?php
                    $args = array(
                        'post_type' => 'white-paper',
                        'posts_per_page' => -1 ,
                        'order' => 'ASC', 

                  );
                    $project = new WP_Query( $args );
                    if( $project->have_posts() ) {
                        ?>
                        <select name="white-paper-dropdown">
                            <option vlaue="0">Select White Paper</option>
                           <?php
                           
                            while( $project->have_posts() ) {
                                $project->the_post();
                                    $dropdown_selected_value = get_post_meta($object->ID, "white-paper-dropdown", true);
                                ?>
                                  <option value="<?php echo get_the_ID();?>" <?php if( $dropdown_selected_value == get_the_ID()){ echo "selected" ;}?>><?php echo get_the_title();  ?></option>
                                <?php

                                }
                                ?>
                        </select>
                        <?php
                       
                        
                    }
                    ?>  
                       
                        
                    <?php
                }
                ?>
                <br>
                <label for="meta-box-checkbox">Case Study</label>
                <?php
                /* Case Study */
                if($checkbox_casestudy_value  == "")
                {
                    ?>
                        <input name="case-study-checkbox" type="checkbox" value="true">
                    <?php
                }
                else if($checkbox_casestudy_value  == "true")
                {
                    ?>  
                        <input name="case-study-checkbox" type="checkbox" value="true" checked>
                        
                    <?php
                }
            ?>
        </div>
    <?php  
}

function job_list( $atts ) {
    $args = array(
          'post_type' => 'job',
          'posts_per_page' => -1,  
          // all posts
    );
    $project = new WP_Query( $args );
    if( $project->have_posts() ) {
        $output .= ' <div class="job-list">
                       <div class="job-contents">
                       <ul>';

       
        
          while( $project->have_posts() ) {
            $project->the_post();
                 $job_address = get_post_meta(get_the_ID(), "wpcf-job-address", true);
                $output .= '<li>
                               <div class="col-md-3 job-title">'.get_the_title().'</div>
                               <div class="col-md-3 job-address">'.$job_address.'</div>
                               <div class="col-md-3 read-more"><a href="'.get_the_permalink().'"><i class="vc_btn3-icon fa fa-angle-double-right"></i> Read More</a></div>
                               <div class="col-md-3 apply-today"><a href="'.get_site_url().'/company/join-us/apply"><i class="vc_btn3-icon fa fa-angle-double-right"></i> Apply Now</a></div>
                            </li>';
          }
         
        $output .= '</ul></div></div>'; 
        }else{
            $output = 'Please add solution from backend';
        }
     return $output;
}
add_shortcode( 'job_list', 'job_list' );

/* Save the Meta box Casetion */
function save_custom_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "post";
    if($slug != $post->post_type)
        return $post_id;

    
    $meta_box_checkbox_value = "";

   /* Save White Paper */
    
    if(isset($_POST["white-paper-checkbox"]))
    {
        $meta_box_whitepaper_value = $_POST["white-paper-checkbox"];
        
        
    }
    if(isset($_POST["white-paper-dropdown"]))
        
    {
        $meta_whitepaper_dropdown_value = $_POST["white-paper-dropdown"];
       
    }
    

    
    /* Save Case Study */
   if(isset($_POST["case-study-checkbox"]))
    {
        $meta_box_casestudy_value = $_POST["case-study-checkbox"];
    }   
    update_post_meta($post_id, "white-paper-checkbox", $meta_box_whitepaper_value);
    update_post_meta($post_id, "case-study-checkbox", $meta_box_casestudy_value);
     update_post_meta($post_id, "white-paper-dropdown", $meta_whitepaper_dropdown_value);
}

add_action("save_post", "save_custom_meta_box", 10, 3);



/* Short code for displaying the white paper and case study in fornt */


/*function whitepaper_casestudy_func(){
    $dropdown_selected_value = get_post_meta(get_the_ID(), "white-paper-dropdown", true);
    
 //   var_dump($dropdown_selected_value);
    
}
add_shortcode( 'download_list', 'whitepaper_casestudy_func' );*/

function article_list_func($atts){
     $atts = shortcode_atts(
		array(
			'post-per-page' => 'post-per-page',
                        'post-per-page' => 'default -1',
			
		), $atts, 'article_list' );

     
     $output .= '<div id="article-list-shortcode">';
                            
                            $type = 'article';
                            $args=array(
                            'post_type' => $type,
                            'post_status' => 'publish',
                            'posts_per_page' => $atts['post-per-page'],
                            'caller_get_posts' => 1,
                            'orderby' => 'date',
                            'order' => 'DESC', 
                            );

                            $my_query = null;
                            $my_query = new WP_Query($args);
                            if ($my_query->have_posts()) {
                                while ($my_query->have_posts()) : $my_query->the_post();

                                $featured_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                                
                              $output .='<div class="wpb_text_column wpb_content_element wpb_cust_article col-md-4 article-content">
                                <div class="wpb_wrapper">';
                                    if($featured_image != NULL):
                                       $output .='<p><img class="alignnone size-full" src="'.$featured_image.'" /></p>';
                                        endif;
                                           
                                        $article_live_url = get_post_meta(get_the_ID(),'wpcf-article-url');
                                       
                                       if($article_live_url[0] != NULL ): 
                                            $output .='<h3><a target="_blank" href="'.$article_live_url[0].'">'.get_the_title().'</a></h3>';
                                            else: 
                                           $output .='<h3><a target="_blank" href="#">'.the_title().';</a></h3>';
                                       endif;

                                    $output .= get_the_date();
                                $output .='</div>
                            </div>';
                            
                                    endwhile;
                                }
                                wp_reset_query();  // Restore global post data stomped by the_post().
                            
			$output .='</div>';
     return $output;
 }
add_shortcode( 'article_list', 'article_list_func' ); 


 

/* Shortcode for listing the pressrealse */
function pressreleases_list_func(){
     
     $output .= '<div id="pressrealese-shortcode">';
                            
                            $type = 'pressrelease';
                            $args=array(
                            'post_type' => $type,
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                            'order'     => 'ASC',
                            
                            );
                            
                            $my_query = null;
                            $my_query = new WP_Query($args);
                            if ($my_query->have_posts()) {
                                while ($my_query->have_posts()) : $my_query->the_post();

                            
                               
                            $output .= '<div class="wpb_text_column wpb_content_element wpb_cust_article article-content">
                                            <div class="wpb_wrapper">';
                                                
					
                                            $output .= '<h3><a target="_blank" href="'.get_the_permalink().'">'.get_the_title().'</a></h3>';
                                            
                                           
                                        $output .= get_the_date();
                                        
                                    
                                $output .='</div>
                            </div>';
                            
                                    endwhile;
                                }
                                wp_reset_query();  // Restore global post data stomped by the_post().
                           
                           
                          
                            
			$output .='</div>';
     return $output;
 }
add_shortcode( 'pressreleases_list', 'pressreleases_list_func' ); 