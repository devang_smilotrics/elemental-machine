<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'elemeoz6_em_live');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}$_6+PFt!fXJeZ8kaU]IGMMSz|_rJ$^@&Rg=|.4II+i$a9hJ^7jsub2LhL/}=)[8');
define('SECURE_AUTH_KEY',  'RL0Q+8eP%/7s?&<|] FNm8c :1f~^5y;c0bJN_:.$0/X@ ~k,%q9tqF(LX10OZUi');
define('LOGGED_IN_KEY',    'th8@1;TtuISW0@a}b#PWlWt%I:|@emCk=~v4yBG[hI)6juQ0mI0Yd+c,w}<D^Guq');
define('NONCE_KEY',        'RJw{82 ?-VaG0Mn7Mk7d,B:Z_`rq`Qn~DTZ<8Y2x|xFboefYLg+ hJv1tHBuM~2,');
define('AUTH_SALT',        '^2~fsO/MI{_>$iP&iQA_isP+~8-PAqQO<L!NtlYOK24g*bYdWspJS$Qz8mzc1wY[');
define('SECURE_AUTH_SALT', 'bfKF;~Vm/D@BYXpKV}C/Ah*c@gZ~b($dbNBH4:[Lt0:/8~D2nW,dqK-R({~xoL0>');
define('LOGGED_IN_SALT',   '`Bis}+s}GE1s>vT~#**9t!D$S1.@V.gz7l1G1?{|vd8:t @$QQBB|0.V832pV6Yv');
define('NONCE_SALT',       'o~#=zGr90ePK(En9?qa=VZhv.;V=y|<w=n>wv^IaFE[gLs[M7}SXq0|A:IVM(A`y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'elemental_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
